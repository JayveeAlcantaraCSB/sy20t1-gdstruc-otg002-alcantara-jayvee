package com.company;

public class PlayerLinkedList {
    private PlayerNode head;
    private int listSize;

    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }
    //Quiz 2 Number 1
    public void removeHead()
    {
        PlayerNode toRemove = head;
        head = toRemove.getNextPlayer();
        toRemove.setNextPlayer(null);
    }

    public void printList()
    {
        listSize = 0;
        PlayerNode current = head;
        System.out.println("Head ->" );
        while(current != null)
        {
            System.out.println(current.getPlayer());
            System.out.println(" -> ");
            current = current.getNextPlayer();
            //Quiz 2 Number 2
            listSize++;
        }
        System.out.println("null");
        System.out.println("List size: " + listSize);
    }
}
