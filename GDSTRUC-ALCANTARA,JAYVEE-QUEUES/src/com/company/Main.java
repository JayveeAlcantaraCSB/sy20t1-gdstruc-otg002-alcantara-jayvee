package com.company;
import java.util.Random;
import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
	ArrayQueue queue = new ArrayQueue(8);
	int x;
	int gamesPlayed = 0;
	int queueNum = 0;
	Player tobeAdded;
	Random rand = new Random();
	Scanner scanner = new Scanner(System.in);
	//List of players
	queue.add(new Player(1, "Shroud", 100));
	queue.add(new Player(2, "DrLupo", 100));
	queue.add(new Player(3, "Ninja", 100));
	queue.add(new Player(4, "CouRage", 100));
	queue.add(new Player(5, "Tarik", 100));
	queue.add(new Player(6, "Summit1g", 100));
	queue.add(new Player(7, "DrDisrespect", 100));

		do
		{
			x = 1 + rand.nextInt(7);
			//intro of game; Checking the number of people in queue (Must be at least 5)
			System.out.println("Queue will start. " + x + " number of people in queue");
			queueNum += x;
			if(queueNum <5)
			{
				System.out.println((5 - queueNum) + " more people needed in queue. Queueing more people...");
				queueNum += (5 - queueNum);
			}
				System.out.println("Queue is full. Game will start");
			String inputPause = scanner.nextLine();

			//Pop the first 5 players for the game while simultaneously re-adding them at the back of the queue for continuity of the game.
			System.out.println("Game commencing! Our list of players: \n");
			for(int i = 0; i < 5; i++)
			{
				System.out.println(queue.peek());
				tobeAdded = queue.remove();
				queue.add(tobeAdded);
			}
			System.out.println("Game has ended. Starting another Queue...\n");
			String inputPause2 = scanner.nextLine();
			clearScreen();
			queueNum = 0;
			gamesPlayed++;
		}
		while(gamesPlayed !=10);
		System.out.println("10 games has been played. Thanks for playing!");

    }
	public static void clearScreen()
	{
		System.out.print("\033[H\033[2J");
		System.out.flush();
	}
}
