package com.company;
import java.util.Random;
import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        //Generating the 3 Random Commands: Draw, Discard, Get x Card from Pile
        Random randomNumber = new Random();

        //Initializing the Deck, Hand, and Discarded Pile Stack
        CardStack playerDeck = new CardStack(30);
        CardStack playerHand = new CardStack(30);
        CardStack discardedPile = new CardStack(30);

        //Initializing Deck manually
        playerDeck.push(new Card("Summit1G"));
        playerDeck.push(new Card("Shroud"));
        playerDeck.push(new Card("m0e"));
        playerDeck.push(new Card("Aceu"));
        playerDeck.push(new Card("Relyks"));
        playerDeck.push(new Card("Wardell"));
        playerDeck.push(new Card("Hiko"));
        playerDeck.push(new Card("TenZ"));
        playerDeck.push(new Card("Braxton"));
        playerDeck.push(new Card("Stewie2k"));
        playerDeck.push(new Card("Skadoodle"));
        playerDeck.push(new Card("Just1n"));
        playerDeck.push(new Card("Chocotaco"));
        playerDeck.push(new Card("Pokimane"));
        playerDeck.push(new Card("QuarterJade"));
        playerDeck.push(new Card("xQc"));
        playerDeck.push(new Card("DrLupo"));
        playerDeck.push(new Card("Singsing"));
        playerDeck.push(new Card("Gladd"));
        playerDeck.push(new Card("Diviny"));
        playerDeck.push(new Card("Mikauto"));
        playerDeck.push(new Card("Vyminal"));
        playerDeck.push(new Card("Noctile"));
        playerDeck.push(new Card("Maktuh"));
        playerDeck.push(new Card("39Daph"));
        playerDeck.push(new Card("Ninja"));
        playerDeck.push(new Card("Pewdiepie"));
        playerDeck.push(new Card("Sykkuno"));
        playerDeck.push(new Card("DisguisedToast"));
        playerDeck.push(new Card("DrDisrespect"));

        int handCounter = 0;
        int deckCounter = 30;
        int pileCounter = 0;
        int x = 0;

        while(deckCounter > 0)
        {
            int randomCommand = randomNumber.nextInt(3);
            if(randomCommand == 0)
            {
                System.out.println("Player draws card/s ");
                if(deckCounter <= 5 )
                {
                    x = deckCounter;
                }
                else{ x = randomNumber.nextInt(5);}
                //Getting card from deck
                for(int i = 0; i < x ; i++)
                {
                    playerHand.push(playerDeck.pop());
                    handCounter++;
                    deckCounter--;
                }
            }
            else if(randomCommand == 1)
            {
                System.out.println("Player discards card/s");
                if(playerHand.isEmpty())
                {
                    System.out.println("Hand is Empty");
                }
                else if(handCounter <=5 )
                {
                    x = handCounter;
                    for(int i = 0; i < x ; i++)
                    {
                        discardedPile.push(playerHand.pop());
                        handCounter--;
                        pileCounter++;
                    }
                }
                else
                { x = randomNumber.nextInt(5);

                    for(int i = 0; i < x ; i++)
                    {
                        discardedPile.push(playerHand.pop());
                        handCounter--;
                        pileCounter++;
                    }}
                //Discarding card from hand to pile
            }
            else if(randomCommand == 2)
            {
                System.out.println("Player gets card/s from pile");
                if(discardedPile.isEmpty())
                {
                    System.out.println("Discarded Pile is Empty");
                }
                else if(pileCounter <= 5)
                {
                    x = pileCounter;

                    for(int i = 0; i < x ; i++)
                    {
                        playerHand.push(discardedPile.pop());
                        handCounter++;
                        pileCounter--;
                    }
                }
                else
                {x = randomNumber.nextInt(5);
                    for(int i = 0; i < x ; i++)
                    {
                        playerHand.push(discardedPile.pop());
                        handCounter++;
                        pileCounter--;
                    }
                }
            }
            String inputPause = scanner.nextLine();
            //Stat Printout
            System.out.println("Stats: \n");
            System.out.println("Cards at hand: \n");
            playerHand.printStack();
            System.out.println("\n");
            System.out.println("Cards on deck: " + deckCounter );
            System.out.println("\n");
            System.out.println("Cards on pile: " + pileCounter );
            System.out.println("\n");
            clearScreen();

            

        }
        System.out.println("Game over, deck is empty");
    }
    //Clearing Screen
    public static void clearScreen()
    {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
