package com.company;

public class Main {

    public static void main(String[] args) {
	int[] numbers = new int[6];
	numbers[0] = 5;
	numbers[1] = 10;
    numbers[2] = 2;
    numbers[3] = 1;
    numbers[4] = 0;
    numbers[5] = 11;

    System.out.println("Before Selection Sort: \n");
    printArray(numbers);
    System.out.println("\n After Selection Sort: \n");
    selectionSort(numbers);
    printArray(numbers);


    }

    private static void bubbleSort(int[] arr)
    {
        //Descending order Bubble Sort
        for(int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for(int i = 0; i < lastSortedIndex; i++)
            {
                if(arr[i] < arr[i +1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
            }
        }
    }

    private static void selectionSort(int[] arr)
    {
        //Descending Order Selection Sort; Prioritizing smallest number to be put at the end of the array
        for(int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int smallestIndex = 0;
            for(int i = 1; i <= lastSortedIndex; i++)
            {
                if(arr[i] < arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }
            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }

    }

    private static void printArray(int[] arr)
    {
        for (int j : arr) {
            System.out.println(j);
        }
    }
}


